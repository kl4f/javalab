package zz.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class LinesBatchConsumer implements Consumer {

    protected int batchSize = 1;
    protected List<String> lst = new ArrayList<>();

    /**
     * operates on field lst.
     */
    public abstract void proceed();

    public void take(String str) {
        this.lst.add(str);
        flush();
    }

    public int flush() {
        return flush(false);
    }

    public int flush(boolean force) {
        if (force || lst.size() >= batchSize) {
            proceed();
            int flushCnt = lst.size();
            this.lst = new ArrayList<>();  // clear lst
            return flushCnt;
        } else
            return 0;
    }
}
