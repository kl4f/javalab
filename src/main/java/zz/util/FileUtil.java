package zz.util;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.*;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FileUtil<T> {
    public static List<String> readAllLines(String fileAbsFn) {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileAbsFn))) {
            list = br.lines().collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<String> readAllLinesIgore(String fileFnAbs) {
        List<String> allLines = readAllLines(fileFnAbs);
        List<String> ret = new ArrayList<>();
        for (String line : allLines) {
            String str = line.trim();
            if (str.length() == 0 || str.startsWith("#")) // ignore blank and comment lines
                continue;
            ret.add(str);
        }
        return ret;
    }

    @SneakyThrows
    public static String[] readFileLinesFromClassPath(String res) {
        List<String> lst = new ArrayList<>();
        URL resUrl = FileUtil.class.getResource(res);
        InputStream is = resUrl.openStream();
        InputStreamReader rdr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(rdr);
        String line;
        while ((line = br.readLine()) != null)
            lst.add(line);
        return lst.toArray(new String[]{});
    }

    @SneakyThrows
    public static void writeFile(String fnAbs, List<String> rows, boolean append) {
        File file = new File(fnAbs);
        if (!file.exists())
            file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile(), append);
        BufferedWriter bw = new BufferedWriter(fw);
        for (String row : rows)
            bw.write(row + "\n");
        bw.close();
        fw.close();
    }

    /**
     * write to new file. If existed, overwrite.
     * @param fnAbs
     * @param rows
     */
    public static void writeFile(String fnAbs, List<String> rows) {
        writeFile(fnAbs, rows, false);
    }

    public static void appendToFile(String fnAbs, List<String> rows) {
        writeFile(fnAbs, rows, true);
    }

    public static List<File> listDir(String dir) {
        List<File> files = new ArrayList<>();
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(dir))) {
            for (Path path : dirStream)
                files.add(path.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }

    public static List<File> listFileByWildcard(String dir, String fn) {
        Collection<File> files = FileUtils.listFiles(new File(dir), new WildcardFileFilter(fn), null);
        return files.stream().collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<Path> listFiles(String dir, DirectoryStream.Filter<Path> filter, Comparator comparator) {
        List<Path> files = new ArrayList<>();
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(dir), filter)) {
            for (Path path : dirStream)
                files.add(path);
        }
        Collections.sort(files, comparator);
        return files;
    }

    public static DirectoryStream.Filter<Path> logFilter() {
        DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept(Path entry) throws IOException {
                if (entry.toString().contains(".log."))
                    return true;
                return false;
            }
        };
        return filter;
    }

    public static boolean mkdir(String newDir) {
        try {
            Files.createDirectories(Paths.get(newDir));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static <T> void consumeFile(String fnAbs, Consumer<String> consumer) {
        try (FileInputStream fis = new FileInputStream(fnAbs)) {
            Scanner scanner = new Scanner(fis, "UTF-8");
            while (scanner.hasNextLine())
                consumer.accept(scanner.nextLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((LinesBatchConsumer) consumer).flush(true);
    }

    public static String fineOneFileByWildcard(String dir, String wildcard) {
        List<File> lst = listFileByWildcard(dir, wildcard);
        if (lst.size() != 1)
            return "";
        else
            return lst.get(0).getAbsolutePath();
    }

    public static void main(String[] args) {
        String fnAbs = "c:\\applogs\\del.txt";
        appendToFile(fnAbs, Arrays.asList("l1-", "l2"));
        listDir("c:\\applogs").forEach(System.out::println);
        listFileByWildcard("c:\\applogs", "*.txt").forEach(System.out::println);
    }
}
