package zz.util.orgs;

import zz.util.FileUtil;
import zz.util.TstUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrgLoader {

    /**
     *
     * @param allLines
     * @param sectTag  starts with *, e.g., "** sect B"
     * @return the specified section, incl sub-levels
     */
    public static List<String> extractSection(List<String> allLines, String sectTag) {
        String lead = sectTag.substring(0, sectTag.indexOf(" ") + 1);
        List<String> retLst = new ArrayList<>();
        boolean start = false;
        for (String line : allLines) {
            if (!start) {
                if (line.trim().equals(sectTag)) {
                    start = true;
                    retLst.add(line);
                }
                continue;
            }

            if (line.startsWith(lead)   // same level
                    || line.startsWith("*") && line.indexOf(" ") < lead.indexOf(" ")  // upper level
            )
                break;
            retLst.add(line);
        }
        return retLst;
    }

    public static void main(String[] args) {
        String fnAbs = TstUtil.getHomeDir() + "/git/orgpub/emacs/common-keys.org";
        List<String> aa = FileUtil.readAllLines(fnAbs);
        List<String> bb = OrgLoader.extractSection(aa, "** magit-status");
        bb = OrgLoader.extractSection(aa, "* Tree");
        bb.forEach(System.out::println);
    }
}
