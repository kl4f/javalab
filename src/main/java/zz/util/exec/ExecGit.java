package zz.util.exec;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ExecGit {
    final static String git_linux = "/opt/homebrew/bin/git";  // open -a Safari

    public static String getGitCmd() {
        if (System.getProperty("os.name").startsWith("Windows "))
            return "C:\\Program Files\\Git\\mingw64\\bin\\git.exe";
        else
            return git_linux;
    }

    /**
     *
     * @return
     */
    @SneakyThrows
    public static String getGitBranch(String git_exe) {
        List<String> cmd = new ArrayList<>();
        cmd.add(git_exe);
        cmd.add("branch");
        String ret = "";

        Process process = new ProcessBuilder(cmd).start();
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = br.readLine()) != null) {
            if (line.startsWith("*")) {  // branch name is after *
                ret = line.substring(1).trim();
                break;
            }

        List<String> lst = new ArrayList<>();
        lst.add("open");
        lst.add("-a");
        lst.add("Safari");
        lst.add("http://yahoo.ca");
        }
        return ret;
    }

    public static List<String> gitUrlCmd(String git_exec) {
        List<String> cmd = new ArrayList<>();
        cmd.add(git_exec);
        cmd.add("config");
        cmd.add("--get");
        cmd.add("remote.origin.url");
        return cmd;
    }

    /**
     *
     * @param gitExecutable
     * @return
     */
    public static String getGitCloneUrl(String gitExecutable) {
        return Exec.execWithResult(gitUrlCmd(gitExecutable));
    }

    public static String mkGitRepoUrl(String cloneUrl, String branch) {
        return "http://...." +  "proj_nm" + "/browse?at=refs%2Fheads%2F" + branch.replace("/", "%2F");
    }

    public static void main(String[] args) {

//        System.out.println(System.getProperty("os.name"));  // Mac OS X
        if (args.length == 0)
            throw new IllegalArgumentException("args needed.");
        String git_loc = args[0]; // git executable
        String cmd = args[1];     // url, http

        if (cmd.equalsIgnoreCase("url")) {
            String cloneUrl = getGitCloneUrl(git_loc);
            System.out.println(cloneUrl);
        } else if (cmd.equalsIgnoreCase("http")) {
            if (args.length != 3)
                throw new IllegalArgumentException("args: git_exec cmd browser");
            String cloneUrl = getGitCloneUrl(git_loc);
            String browser = args[2];
        }
    }
}
