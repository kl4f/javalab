package zz.util.exec;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Exec {
    final static String chrome_exe = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";

    /**
     * get current dir. Used for cmd line.
     * @return
     */
    public static String pwd() {
        return Paths.get(".").toAbsolutePath().normalize().toString();
    }

    public static void openUrlInChrome(String url) {
        String osNm = System.getProperty("os.name");  // Windows, Mac OS X

        List<String> cmd = new ArrayList<>();
        if (osNm.startsWith("Windows "))
            cmd.add(chrome_exe);
        else if (osNm.startsWith("Mac OS")) {
            cmd.add("open");
            cmd.add("-a");
            cmd.add("Google Chrome");
        }
        cmd.add("-new-tab");
        cmd.add(url);
        exec(cmd);
    }

    @SneakyThrows
    public static void exec(List<String> cmd) {
        new ProcessBuilder(cmd).start();
    }

    @SneakyThrows
    public static String execWithResult(List<String> cmd) {
        String ret = "";
        Process process = new ProcessBuilder(cmd).start();
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = br.readLine()) != null)
            ret += line;
        return ret;
    }

    public static void main(String[] args) {
        System.out.println(pwd());

//        List<String> lst = new ArrayList<>();
//        lst.add("open");
//        lst.add("-a");
//        lst.add("Safari");
//        lst.add("http://yahoo.ca");
//        exec(lst);
    }
}
