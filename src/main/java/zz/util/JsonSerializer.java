package zz.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

public class JsonSerializer {

    @SneakyThrows
    public <E> String serialize(E obj) {
        return new ObjectMapper().writeValueAsString(obj);
    }

    @SneakyThrows
    public <E> E deserialize(Class <E> type, String jsonStr) {
        return new ObjectMapper().readValue(jsonStr, type);
    }
}
