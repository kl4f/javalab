package zz.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {
    public static <T> T jsonToObj(String jsonStr, Class<T> cls) {
        try {
            return new ObjectMapper().readValue(jsonStr, cls);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String jsonUpdate(String jsonStr, Map<String, Object> map) {
        System.out.println(jsonStr);
        JsonObject obj = JsonParser.parseString(jsonStr).getAsJsonObject();
        map.forEach((k, v) -> {
            if (v instanceof String)
                obj.addProperty(k, v.toString());
            else
                obj.add(k, JsonParser.parseString(v.toString()));
        });
        return obj.toString();
    }

    public static void main(String[] args) {
        String jsonStr = "{\"name\": \"a\", \"age\": 17}";
        Map<String, Object> map = new HashMap<>();
        map.put("gender", "F");
        map.put("age", 18);
        String result = jsonUpdate(jsonStr, map);
        System.out.println(result);
    }
}
